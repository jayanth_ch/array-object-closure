const testobject = { name: 'jayanth', place: 'ongole', branch: 'ece' };
const testobject2 = { hero: 'Tony', email: 'tonystark@ironman' };

const { keys, values, mapobject, pairs, invert, defaults } = require('./Object');


var key = keys(testobject);
console.log("---keys");
console.log(key);


var val = values(testobject);
console.log("---values");
console.log(val);


var mapobjects = mapobject(testobject, (val, key) => val + 9);
console.log("---mapobject");
console.log((mapobjects));


var pairobjects = pairs(testobject);
console.log("---pairs");
console.log(pairobjects);


var invertobjects = invert(testobject);
console.log("---invert");
console.log(invertobjects);

//console.log(testObject)


var defaultobject = defaults(testobject, testobject2);
console.log("---defaults");
console.log(defaultobject);
