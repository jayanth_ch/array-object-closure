//inputs
const items = [1, 2, 3, 4, 5];
const nestedArray = [1, [2], [[3]], [[[4]]]];


const { myeach, mymap, myreduce, myfind, myfilter, myflatten } = require('./Array');


// each function
console.log('---each---');
myeach(items, (element, index) => console.log(element, index));

//  map function
var mappedarray = mymap(items, (element) => element * 2);
console.log('---map---');
console.log(mappedarray);

// reduce function
var reduceValue = myreduce(items, (a, b) => a + b, 5);
console.log('---reduce---');
console.log(reduceValue);

//  find function
var find = myfind(items, (a) => a % 2 === 0);
console.log('---find---');
console.log(find);

// filter function
var filterarray = myfilter(items, (element) => element % 2 === 0);
console.log('---filter---');
console.log(filterarray);

// flatten function
var flattenarray = myflatten(nestedArray);
console.log('---flatten---');
console.log(flattenarray);
