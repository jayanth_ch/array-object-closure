// count fn
function counter() {
    let count = 0;
    return {
        increment() {
            return ++count;
        },
        decrement() {
            return --count;
        }
    }
}


//  limitFunctionCallCount fn
function callcount(cb, n) {
    let counter = 0;
    function invoke() {
        if (counter < n) {
            counter++
            return cb();
        } else {
            return null;
        }
    }
    return invoke;
}

// cache fn
function cachefn(cb) {
    let cache = {};

    function invoke(arg) {
        if (arg in cache) {
            return cache[arg];
        }
        else {
            cache[arg] = cb(arg);
            return cache[arg];
        }
    }

    return invoke;
}


module.exports = { counter, callcount, cachefn };
