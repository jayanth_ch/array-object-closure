
// keys functions to obtain keys
// array is the object
function keys(array) {
    let keys = [];
    for (let key in array) {
        keys.push(key);
    }
    return keys;
}

// values function to obtain values
function values(array) {
    let values = [];
    for (let key in array) {
        values.push(array[key]);
    }
    return values;
}

// mapobject fn
function mapobject(array, cb) {
    for (let key in array) {
        let changed = cb(array[key], key, array);
        array[key] = changed;
    }
    return array;
}

// pairs fn
function pairs(array) {
    let pairedarray = [];
    for (let key in array) {
        pairedarray.push([key, array[key]]);
    }
    return pairedarray;
}

// invert fn
function invert(array) {
    let newarray = {}
    for (let key in array) {
        newarray[array[key]] = key;
    }
    return newarray;
}

// defaults fn
function defaults(array, defaultProps) {
    for (let key in defaultProps) {
        if (!(key in array)) {
            array[key] = defaultProps[key];
        }
    }
    return array;
}


module.exports = { keys, values, mapobject, pairs, invert, defaults }
