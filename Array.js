// created each function
// key is i
function myeach(elements, cb) {
    for (let i = 0; i < elements.length; i++) {
        cb(elements[i], i);
    }
}

// map function created
function mymap(elements, cb) {
    let newarray = [];
    for (let i = 0; i < elements.length; i++) {
        let value = cb(elements[i], i, elements);
        newarray.push(value);
    }
    return newarray;
}


// reduce function created
function myreduce(elements, cb, startingvalue) {
    let result = startingvalue ? startingvalue : elements[0];
    if (startingvalue) {
        for (let i = 0; i < elements.length; i++) {
            result = cb(result, elements[i], i, elements);
        }
    } else {
        for (let i = 1; i < elements.length; i++) {
            result = cb(result, elements[i], i, elements);
        }
    }
    return result;
}


// find function created
function myfind(elements, cb) {
    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i])) {
            return elements[i];
        }
    }
    return undefined;
}


// filter function created
function myfilter(elements, cb) {
    let newarray = [];
    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i])) {
            newarray.push(elements[i]);
        }
    }
    return newarray;
}



// flatten function created
function myflatten(elements) {
    let newarray = [];
    for (let item of elements) {
        if (Array.isArray(item)) {
            newarray = newarray.concat(myflatten(item));
        } else {
            newarray.push(item);
        }
    }

    return newarray;
}


module.exports = { myeach, mymap, myreduce, myfind, myfilter, myflatten }
