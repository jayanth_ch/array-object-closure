const { counter, callcount, cachefn } = require('./Closure');


// calling the counter fn
let a = counter();
console.log('---counter');
console.log(a.decrement());

// calling the limitFunctionCallCount
const limitfunction = callcount(() => "helloworld", 4);
console.log('---limitfunction');
console.log(limitfunction());

//calling the cache fn
const result = cachefn(n => n);
console.log(result(1));
console.log(result(2));
console.log(result(3));
console.log(result(4));



